locals {
    common_tags = {
        Service = "Storage"
        ManagedBy = "Terraform"
        Environment = var.environment 
        Owner = "FM"
    }

    ip_filepath = "ips.json"
}